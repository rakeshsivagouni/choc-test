"""
This sample demonstrates an implementation of the Lex Code Hook Interface
in order to serve a sample bot which manages reservations for hotel rooms and car rentals.
Bot, Intent, and Slot models which are compatible with this sample can be found in the Lex Console
as part of the 'BookTrip' template.

For instructions on how to set up and test this bot, as well as additional samples,
visit the Lex Getting Started documentation http://docs.aws.amazon.com/lex/latest/dg/getting-started.html.
"""

import json
import datetime
import time
import os
import dateutil.parser
import logging
import sys
from botocore.vendored import requests


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)









def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }


def confirm_intent(session_attributes, intent_name, slots, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_name,
            'slots': slots,
            'message': message
        }
    }


def close(session_attributes, fulfillment_state, message):
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response



def delegate(session_attributes, slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }




# --- Helper Functions ---


def safe_int(n):
    """
    Safely convert n value to int.
    """
    if n is not None:
        return int(n)
    return n


def try_ex(func):
    """
    Call passed in function in try block. If KeyError is encountered return None.
    This function is intended to be used to safely access dictionary.

    Note that this function would have negative impact on performance.
    """

    try:
        return func()
    except KeyError:
        return None


""" --- Functions that control the bot's behavior --- """

def get_login(intent_request):
    
    loginname = try_ex(lambda: intent_request['currentIntent']['slots']['user_name'])
    
    pwd       = try_ex(lambda: intent_request['currentIntent']['slots']['pass_word'])
    
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
    apiurl ='https://6yr7p2kzfi.execute-api.us-east-1.amazonaws.com/Test/login'
    
    data = {
    "loginuser"    :loginname,
    'password'     :pwd 
    }
    
    loginresponse = requests.post(url= apiurl,data= json.dumps(data))
    
    global login_number
    

    for i in loginresponse.json():
        dictt = i
        for j in dictt:
            if j == "login_id":
                login_number = dictt[j]
                success_response = "Authentication Successful"
            else :
                continue
            
    return close(
       session_attributes,
       'Fulfilled',
       {
        'contentType': 'PlainText',
        'content': success_response
       }
    )
    
    
    # For ActionItems

def get_actionitem(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
    # apiurl ='https://0c6b3tmhvk.execute-api.us-east-1.amazonaws.com/Test/finalactionitem'
    
    apiurl ='https://9gjvrnj0i7.execute-api.us-east-1.amazonaws.com/Demo/getfirstactionitem'



    data = {
      'chassignid' : login_number
    }
    
    response = requests.post(url= apiurl,data= json.dumps(data))
    
    list = response.json()
    final = {}
    global currentActionId
    global currentActionItem
    x = 0
    for key,value in list.items():
        for i in value:
            
            for j,k in i.items():
                
                if j == 'chid':
                    
                    currentActionId = k
                    currentActionItem = i['challenge']
                    final=i
                    
            break
    session_attributes['current']  = currentActionId
    
    print(currentActionId)
    
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content':  str(currentActionItem) + 10*' '  +"What would you like to do? 1) Nothing 2) go to next 3)Reassign 4)Change Due Date 5)Delete"
        }
    )


def get_actionitemnothing(intent_request):

   
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
   
    # apiurl ='https://0c6b3tmhvk.execute-api.us-east-1.amazonaws.com/Test/finalactionitem'
    
    apiurl ='https://9gjvrnj0i7.execute-api.us-east-1.amazonaws.com/Demo/getfirstactionitem'



    data = {
    'chassignid' :100
    }
    response = requests.post(url= apiurl,data= json.dumps(data))
    
  
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Thank you'
        }
    )
    
def next_action(intent_request):
    
    logger.info("call the function")
    
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    

    # apiurl ='https://0c6b3tmhvk.execute-api.us-east-1.amazonaws.com/Test/finalactionitem'
    
    apiurl ='https://9gjvrnj0i7.execute-api.us-east-1.amazonaws.com/Demo/getfirstactionitem'

    
    data = {
      'chassignid' : login_number
    }
    
    
    nextresponse = requests.post(url= apiurl,data= json.dumps(data))
    
    
    list1 = nextresponse.json()
    final1 = []
    temp = int(session_attributes['current'])

    global currentActionItem
    
    for key,value in list1.items():
        for i in value:
            for j,k in i.items():
                if j == 'chid':
                    if((temp!=0) and temp<k):
                        if not final1:
                            final1 = k
                            currentActionItem = i['challenge']
                            break
    session_attributes['current']  = final1


    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': str(currentActionItem)+10*' '+"What would you like to do? 1. Nothing 2. go to next 3. Reassign 4. Change Due Date 5. Delete"
        }
    )
    
    
def update_action(intent_request):
    
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    action_date = try_ex(lambda: intent_request['currentIntent']['slots']['changeactiondate'])
    
    reservation = json.dumps({
       
    'changeactiondate': action_date
    
    })
    
    apiurl ='https://6yk0mzwzcl.execute-api.us-east-1.amazonaws.com/Test/actionupdate'
	
    data = {
    "duedatee"  :action_date,
    'chidd'     :currentActionId 
    }
    
    responseupdate = requests.put(url= apiurl,data= json.dumps(data))
    
    
    return close(
       session_attributes,
       'Fulfilled',
       {
        'contentType': 'PlainText',
        'content': 'Done'+ responseupdate.text
       }
    )


def delete_action(intent_request):
    
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    apiurl ='https://h7lb2ghkl5.execute-api.us-east-1.amazonaws.com/Test/actiondelete'
    
    data = {
    'chidd':currentActionId
    }
    
    deleteresponse = requests.delete(url= apiurl,data= json.dumps(data))
    
    return close(
       session_attributes,
       'Fulfilled',
      {
        'contentType': 'PlainText',
        'content': 'Done' + deleteresponse.text
       }
    )
    


def get_duedateactionitems(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    reservation = json.dumps({
        'cheassign': login_number
    })
    
    # apiurl ='https://dv5o4j2ha2.execute-api.us-east-1.amazonaws.com/Test/actionitemduedate'
    
    apiurl ='https://3og2tqz0de.execute-api.us-east-1.amazonaws.com/Demo/actionitemduedate'
    
    data = {
    'chassignid' :login_number
    }
    responseduedate = requests.post(url= apiurl,data= json.dumps(data))
    
    response = responseduedate.json() 
    
    for key,value in response.items():
        for i in value:
            count = i

  
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': str(count)
        }
    )

    #  For Goals


    
def get_goalgroup(intent_request):
    
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    apiurl ='https://439iw8t45g.execute-api.us-east-1.amazonaws.com/Demo/goalsIds'
    
    data = {
        'chassignid':22
    }

    goalsresponse = requests.post(url= apiurl,data= json.dumps(data))


    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            # 'content': goalsresponse.text+10*' '+'Which goal group would you like'
            'content': goalsresponse.text +10*' '+'what would you like to do.1.lookup goals for a goalgroup'

        }
    )
    
    
    
  
def get_cgoals(intent_request):
    

    goalgroupid = try_ex(lambda: intent_request['currentIntent']['slots']['goalgroup'])

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    # apiurl ='https://x9629yuo4a.execute-api.us-east-1.amazonaws.com/Test/cgoals'
    
    apiurl ='https://fmea2lbjp2.execute-api.us-east-1.amazonaws.com/Demo/cgoals'
    
    data = {
        "cgoalgroup":goalgroupid
    }
    
    responseGoals = requests.post(url= apiurl,data= json.dumps(data))
    
    
 
    
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            # 'content': responseGoals.text+10*' '+'Which goalgroup number would you like'
            'content': responseGoals.text+10*' '+'What would you like to do? 1.lookup goal'


        }
    )
    

    
   
    
    
    
def get_singlegoal(intent_request):
    
    
    single_goal = try_ex(lambda: intent_request['currentIntent']['slots']['primary_id'])

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    # apiurl ='https://kb86rogn68.execute-api.us-east-1.amazonaws.com/Test/singlegoal'
    
    apiurl ='https://lb3wj13qc6.execute-api.us-east-1.amazonaws.com/Demo/singlegoal'
    
    data = {
        "cgoal_id":single_goal
    }
    
    responsesinglegoal = requests.post(url= apiurl,data= json.dumps(data))

    
    list1 = responsesinglegoal.json() 
    
    final = {}
    global currentgoalid
    global currentcgoal

    x = 0
    for key,value in list1.items():
        for i in value:
            print(i)
            for j,k in i.items():

                if j == 'cgoal_id':
                    currentgoalid = k
                    currentcgoal = i['cgoals']
                    final=i
            break


    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': currentcgoal+10*' '+'What would you like to do? 1. Change goal status 2. Reassign 3. List active strategies for this goal'
        }
    )
    
    
def changegoalstatus(intent_request):
    
    statusnumber = safe_int(try_ex(lambda: intent_request['currentIntent']['slots']['status_goalid']))

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl ='https://5evlo44b5e.execute-api.us-east-1.amazonaws.com/Test/changegoalstatus'
    
    data = {
         'corpgoalstatus':statusnumber,
         'cgoal_id'      :currentgoalid
    }
    

    responsenext = requests.put(url= apiurl,data= json.dumps(data))
    

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Done'+responsenext.text
        }
    )
    
    
    

    
    
def activestrategyforgoal(intent_request):
    

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    apiurl ='https://f0i0kou5b5.execute-api.us-east-1.amazonaws.com/Test/activestrategiesforgoal'
    
    data = {
        'goal':currentgoalid
    }

    response = requests.post(url= apiurl,data= json.dumps(data))

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            # 'content': response.text +'Which strategy number would you like?'
             'content': response.text +10*' '+'What would you like to do? 1.lookup strategy'

        }
    )
    
    
    
def getactivesinglestrategygoal(intent_request):
    
    
    singleactivegoal  = try_ex(lambda: intent_request['currentIntent']['slots']['active_strategy'])

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    # apiurl ='https://8lbvg66dtj.execute-api.us-east-1.amazonaws.com/Test/SingleActiveStragyGoal'
    
    apiurl ='https://13rl6nwuk5.execute-api.us-east-1.amazonaws.com/Test/SingleActiveStragyGoal'
    
    data = {
        "id":singleactivegoal
    }
    
    responsestrategy = requests.post(url= apiurl,data= json.dumps(data))

    
    activelist = responsestrategy.json() 
    
    final11 = {}
    global currentid
    global currentactivegoal

    x = 0
    for key,value in activelist.items():
        for i in value:
            print(i)
            for j,k in i.items():

                if j == 'id':
                    currentid = k
                    currentactivegoal = i['category']
                    final11=i
            break
        
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            # 'content': responsestrategy.text+10*' '+'What would you like to do? 1. Change strategy status 2. List active initiatives for this strategy'
            'content': currentactivegoal+10*' '+'What would you like to do? 1. Change strategy status 2. List active initiatives for this strategy'
        }
    )

def onholdgoals(intent_request):
    
    
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    
    # apiurl = 'https://pclnsh5ut5.execute-api.us-east-1.amazonaws.com/Test/lexresponseonhold'
    
    apiurl ='https://vij5xpf3zh.execute-api.us-east-1.amazonaws.com/Demo/lexresponseonhold'
   
    data = {
        'ccreatedby':login_number
    }

    response = requests.post(url= apiurl,data= json.dumps(data))

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': response.text+10*' '+'Which goal would you like?'
        }
    )
    
    
def single_goal_onhold(intent_request):
    
    
    onholdsinglegoal = safe_int(try_ex(lambda: intent_request['currentIntent']['slots']['one_goal']))

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    # apiurl ='https://kb86rogn68.execute-api.us-east-1.amazonaws.com/Test/singlegoal'
    
    apiurl ='https://lb3wj13qc6.execute-api.us-east-1.amazonaws.com/Demo/singlegoal'
    
    data = {
        'cgoal_id':onholdsinglegoal
    }
    
    responseGoals = requests.post(url= apiurl,data= json.dumps(data))

    
    list1 = responseGoals.json() 
    
    final = {}
    global currentgoalid
    global currentcgoal

    x = 0
    for key,value in list1.items():
        for i in value:
            print(i)
            for j,k in i.items():

                if j == 'cgoal_id':
                    currentgoalid = k
                    currentcgoal = i['cgoals']
                    final=i
            break

    

 
 
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': currentcgoal+10*' '+'What would you like to do? 1. Change goal status'
        }
    )


def underreviewgoals(intent_request):
    
    

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}


    # apiurl ='https://5hqvrl44ne.execute-api.us-east-1.amazonaws.com/default/underreview'
    
    apiurl ='https://ebr4undald.execute-api.us-east-1.amazonaws.com/Demo/underreview'
    
   
    data = {
        'ccreatedby':login_number
    }

    response = requests.post(url= apiurl,data= json.dumps(data))

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': response.text +10*' '+'Which goal would you like?'
        }
    )

    # For First Initiative
    
def get_first_initiative(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 

    # apiurl = 'https://d6nd129stk.execute-api.us-east-1.amazonaws.com/Test/firststrategy'
    
    apiurl ='https://vuk0cx1cqa.execute-api.us-east-1.amazonaws.com/Demo/firststrategy'

    data = {
        'assignoto':login_number
    }
    

    responseone = requests.post(url= apiurl,data= json.dumps(data))
    
    list1 = responseone.json() 
    
    final = {}
    global currentInitiative
    global firstinitiativeID

    x = 0
    for key,value in list1.items():
        for i in value:
            print(i)
            for j,k in i.items():

                if j == 'id':
                    firstinitiativeID = k
                    currentInitiative = i['initiative']
                    final=i
            break
    session_attributes['current']  = firstinitiativeID
    
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': currentInitiative+10*' '+'What would you like to do? 1.Initiative Nothing 2. show next initiative 3. Change initiative status 4. Reassign 5.Change Due Date For Initiative'
        }
    )
    
    
def get_nothing(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    # apiurl = 'https://d6nd129stk.execute-api.us-east-1.amazonaws.com/Test/firststrategy'
    
    apiurl ='https://vuk0cx1cqa.execute-api.us-east-1.amazonaws.com/Demo/firststrategy'
    
    data = {
        'chassignid':22
    }
    
    responseone = requests.post(url= apiurl,data= json.dumps(data))
 
 
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Thank you'
        }
    )
    
    
def nextfirstinitiative(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    # apiurl = 'https://d6nd129stk.execute-api.us-east-1.amazonaws.com/Test/firststrategy'
    
    apiurl ='https://vuk0cx1cqa.execute-api.us-east-1.amazonaws.com/Demo/firststrategy'
    
    data = {
        'assignoto':login_number
    }
    

    firstresponse = requests.post(url= apiurl,data= json.dumps(data))
    
    list1 = firstresponse.json()
    final1 = []
    nextfirst = int(session_attributes['current'])
    global currentInitiative
    
    for key,value in list1.items():
        for i in value:
            for j,k in i.items():
                if j == 'id':
                    if((nextfirst!=0) and nextfirst<k):
                        if not final1:
                            final1 = k
                            currentInitiative = i['initiative']
                            temp = final1
                            break
    session_attributes['current']  = final1
    

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': currentInitiative+10*' '+'What would you like to do? 1.Initiative Nothing 2. show next initiative 3. Change initiative status 4. Reassign 5.Change Due Date For Initiative'
        }
    )
    

def changefirstinitiativestatus(intent_request):
    
    statusnumber = safe_int(try_ex(lambda: intent_request['currentIntent']['slots']['onestatusid']))

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl='https://lvmdl3vrv0.execute-api.us-east-1.amazonaws.com/Test/changeinitiativestatus'
    
    data = {
         'initiativestatusid':statusnumber,
         'id'                :firstinitiativeID
    }
    

    responsenext = requests.put(url= apiurl,data= json.dumps(data))
    

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Done'+responsenext.text
        }
    )
    
    
    
def change_duedate(intent_request):
    

    changeInidate = try_ex(lambda: intent_request['currentIntent']['slots']['firstduedate'])
    
    print(changeInidate)

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
    reservation = json.dumps({
        
        'firstduedate': changeInidate
    })
    
    
    apiurl ='https://u5y1nawp2k.execute-api.us-east-1.amazonaws.com/Test/changeduedateinitiative'


    data = {
        'duedatee' :changeInidate,
        'id'       :firstinitiativeID
    }
    responseduedate = requests.put(url= apiurl,data= json.dumps(data))
  
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content':'Done'+ responseduedate.text
        }
    )
    
    
    # For Second Initiative
    

def get_secondinitiative(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    # apiurl = 'https://s7tnax82ui.execute-api.us-east-1.amazonaws.com/Test/secondstrategy'
    
    apiurl ='https://dtp89itfgk.execute-api.us-east-1.amazonaws.com/Demo/demosecondstrategy'
    
    data = {
        'assignoto':login_number
    }
    

    responseone = requests.post(url= apiurl,data= json.dumps(data))
    
    list1 = responseone.json() 
    
    final = {}
    global SecondCurrentInitiative
    global secondcurrentIntiativeid

    x = 0
    for key,value in list1.items():
        for i in value:
            print(i)
            for j,k in i.items():

                if j == 'id':
                    secondcurrentIntiativeid = k
                    SecondCurrentInitiative = i['initiative']
                    final=i
            break
    session_attributes['current']  = secondcurrentIntiativeid



    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': SecondCurrentInitiative+10*' '+'What would you like to do? 1.Second Initiative Nothing 2. show next initiatives 3. change second initiative status 4. Reassign 5.change due date for second initiative'
        }
    )
    
    

def getsecondnothing(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl ='https://2sgz1uv840.execute-api.us-east-1.amazonaws.com/Test/firststrategy'
    
    data = {
        'chassignid':22
    }
    
    responseone = requests.post(url= apiurl,data= json.dumps(data))
 
 
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Thank you'
        }
    )
    

def nextsecondinitiatives(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 

    # apiurl = 'https://s7tnax82ui.execute-api.us-east-1.amazonaws.com/Test/secondstrategy'
    
    apiurl ='https://dtp89itfgk.execute-api.us-east-1.amazonaws.com/Demo/demosecondstrategy'


    data = {
        'assignoto':login_number
    }
    

    secondnext = requests.post(url= apiurl,data= json.dumps(data))
    
    list1 = secondnext.json()
    finalsecond = []
    secondtemp = int(session_attributes['current'])
    global SecondCurrentInitiative

    for key,value in list1.items():
        for i in value:
            for j,k in i.items():
                if j == 'id':
                    if((secondtemp!=0) and secondtemp>k):
                        if not finalsecond:
                            finalsecond = k
                            SecondCurrentInitiative = i['initiative']
                            temp = finalsecond
                            break
    session_attributes['current']  = finalsecond
    

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': SecondCurrentInitiative+10*' '+'What would you like to do? 1. Second Initiative Nothing 2.show next initiatives 3. change second initiative status 4. Reassign 5.change due date for second initiative'
        }
    )
    
    
def changesecondiniti(intent_request):
    
    statusnumber = safe_int(try_ex(lambda: intent_request['currentIntent']['slots']['secondstatusid']))

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl='https://lvmdl3vrv0.execute-api.us-east-1.amazonaws.com/Test/changeinitiativestatus'
    
    data = {
         'initiativestatusid':statusnumber,
         'id'                :secondcurrentIntiativeid
    }
    

    responsenext = requests.put(url= apiurl,data= json.dumps(data))
    

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Done'+responsenext.text
        }
    )
    
    
    
def changesecondduedate(intent_request):
    

    changeInidate = try_ex(lambda: intent_request['currentIntent']['slots']['secondduedate'])
    
    print(changeInidate)

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
    reservation = json.dumps({
        
        'firstduedate': changeInidate
    })
    
    
    apiurl ='https://u5y1nawp2k.execute-api.us-east-1.amazonaws.com/Test/changeduedateinitiative'


    data = {
        'duedatee' :changeInidate,
        'id'       :secondcurrentIntiativeid
    }
    responseduedate = requests.put(url= apiurl,data= json.dumps(data))
  
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content':'Done'+ responseduedate.text
        }
    )
    
    
    # For Third Initiative

def get_thirdinitiative(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
    # apiurl = 'https://3tydbsdn34.execute-api.us-east-1.amazonaws.com/Test/thirdstrategy'
    
    apiurl ='https://dxowy3i9o2.execute-api.us-east-1.amazonaws.com/Demo/thirdstrategy'
    
    data = {
        'assignoto':login_number
    }
    

    responseone = requests.post(url= apiurl,data= json.dumps(data))
    
    list1 = responseone.json() 
    
    final = {}
    global currentInitiative
    global currentIntiativeid

    x = 0
    for key,value in list1.items():
        for i in value:
            print(i)
            for j,k in i.items():

                if j == 'id':
                    currentIntiativeid = k
                    currentInitiative = i['initiative']
                    final=i
            break
    session_attributes['current']  = currentIntiativeid

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            # 'content': currentInitiative+10*' '+'What would you like to do? 1. Nothing 2. Go to Next 3. Change initiative status 4. Reassign 5. Change Due Date'
            'content': currentInitiative+10*' '+'What would you like to do? 1. Third Initiative Nothing 2. show next third initiative 3. change third initiative status 4. Reassign 5. change due date for third initiative'

            
        }
    )
    
    
def getthirdnothing(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl ='https://2sgz1uv840.execute-api.us-east-1.amazonaws.com/Test/firststrategy'
    
    data = {
        'chassignid':22
    }
    
    

    responseone = requests.post(url= apiurl,data= json.dumps(data))
 
 
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Thank you'
        }
    )
    
    

def nextthirdinitiatives(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    # apiurl = 'https://3tydbsdn34.execute-api.us-east-1.amazonaws.com/Test/thirdstrategy'
    
    apiurl ='https://dxowy3i9o2.execute-api.us-east-1.amazonaws.com/Demo/thirdstrategy'

    data = {
        'assignoto':login_number
    }
    

    responsenext = requests.post(url= apiurl,data= json.dumps(data))
    
    list1 = responsenext.json()
    final1 = []
    global currentInitiative
    global nextcurrentInitiative
    global nextcurrnetid
    temp = int(session_attributes['current'])
    global currentInitiative
    
    for key,value in list1.items():
        for i in value:
            for j,k in i.items():
                if j == 'id':
                    if((temp!=0) and temp>k):
                        if not final1:
                            final1 = k
                            currentInitiative = i['initiative']
                            temp = final1
                            break
    session_attributes['current']  = final1
    
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            # 'content': currentInitiative+10*' '+'What would you like to do? 1. Nothing 2. Go to Next 3. Change initiative status 4. Reassign 5. Change Due Date'
            'content': currentInitiative+10*' '+'What would you like to do? 1. Third Initiative Nothing 2.show next third initiative 3. change third initiative status 4. Reassign 5. change due date for third initiative'
        }
    )
    
def changethirdiniti(intent_request):
    
    statusnumber = safe_int(try_ex(lambda: intent_request['currentIntent']['slots']['thirdstatusid']))

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl='https://lvmdl3vrv0.execute-api.us-east-1.amazonaws.com/Test/changeinitiativestatus'
    
    data = {
         'initiativestatusid':statusnumber,
         'id'                :currentIntiativeid
    }
    

    responsenext = requests.put(url= apiurl,data= json.dumps(data))
    

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Done'+responsenext.text
        }
    )
    
    
def changeduedatethird(intent_request):
    

    changeInidate = try_ex(lambda: intent_request['currentIntent']['slots']['thirdduedate'])
    
    print(changeInidate)

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
    reservation = json.dumps({
        
        'firstduedate': changeInidate
    })
    
    
    apiurl ='https://u5y1nawp2k.execute-api.us-east-1.amazonaws.com/Test/changeduedateinitiative'


    data = {
        'duedatee' :changeInidate,
        'id'       :currentIntiativeid
    }
    responseduedate = requests.put(url= apiurl,data= json.dumps(data))
  
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content':'Done'+ responseduedate.text
        }
    )
    


def past_dueinitiatives(intent_request):

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

    # apiurl ='https://3boevs8lp6.execute-api.us-east-1.amazonaws.com/Test/dueinitiative'
    
    # apiurl ='https://ybhawo3suk.execute-api.us-east-1.amazonaws.com/Test/dueinitiative'
    
    apiurl ='https://4g0dqkeob2.execute-api.us-east-1.amazonaws.com/Demo/dueinitiative'

    data = {
        'assignoto':login_number
    }
    
    

    responsedue = requests.post(url= apiurl,data= json.dumps(data))
 
 
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': responsedue.text+10*'  '+'what would you like to do?1.lookup initiative'   
        }
    )
    
    
    

def  one_initiatives(intent_request):
    
    initiative_data = try_ex(lambda: intent_request['currentIntent']['slots']['primaryid'])

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
    apiurl ='https://778u2hqqpl.execute-api.us-east-1.amazonaws.com/Test/initiative'
    
    data = {
        'id':initiative_data
    }
    
    

    responseinitiative = requests.post(url= apiurl,data= json.dumps(data))
    
    list1 = responseinitiative.json() 
    
    final = {}
    global currentinitiativeid
    global currentinitiative

    x = 0
    for key,value in list1.items():
        for i in value:
            print(i)
            for j,k in i.items():

                if j == 'id':
                    currentinitiativeid = k
                    currentinitiative = i['initiative']
                    final=i
            break
    
 
 
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': currentinitiative+10*' '+"What would you like to do? 1. Nothing 2. Change initiative status 3. Reassign 4.Change Due Date"
        }
    )
    

def one_dueinitiatives(intent_request):
    
    
    oneid = safe_int(try_ex(lambda: intent_request['currentIntent']['slots']['idd']))

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl ='https://0n0v5navf9.execute-api.us-east-1.amazonaws.com/Test/pastoneinitiative'
    
    data = {
        'id':oneid
    }
    
    

    responsedue = requests.post(url= apiurl,data= json.dumps(data))
 
 
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': responsedue.text+'What would you like to do? 1. Nothing 2. Change initiative status 3. Reassign'
        }
    )


def changeinitiativestatus(intent_request):
    
    statusnumber = safe_int(try_ex(lambda: intent_request['currentIntent']['slots']['statusid']))

    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
 
    apiurl='https://lvmdl3vrv0.execute-api.us-east-1.amazonaws.com/Test/changeinitiativestatus'
    
    data = {
         'initiativestatusid':statusnumber,
         'id'                :currentIntiativeid
    }
    

    responsenext = requests.put(url= apiurl,data= json.dumps(data))
    

    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Done'+responsenext.text
        }
    )
    
    






# --- Intents ---


def dispatch(intent_request):

    
    intent_name = intent_request['currentIntent']['name']
    
    
    if intent_name == 'goalsinfo':
        return get_goalgroup(intent_request)
    elif intent_name == 'lookupgoalid':
        return get_cgoals(intent_request)
    elif intent_name == 'firststrategy':
        return get_first_initiative(intent_request)
    elif intent_name == 'FirstInitiativeNothing':
        return get_nothing(intent_request)
    elif intent_name == 'nextinitiativestrategy':
        return nextfirstinitiative(intent_request)
    elif intent_name == 'changeduedateinitiative':
        return change_duedate(intent_request)
    elif intent_name == 'listpastdueinitiatives':
        return past_dueinitiatives(intent_request)
    elif intent_name == 'onedueinitiative':
        return one_dueinitiatives(intent_request)
    elif intent_name == 'changeinitiativestatus':
        return changeinitiativestatus(intent_request)
    elif intent_name == 'secondstrategy':
        return get_secondinitiative(intent_request)
    elif intent_name == 'thirdstrategy':
        return get_thirdinitiative(intent_request)
    elif intent_name == 'getaction':
        return get_actionitem(intent_request)
    elif intent_name == 'donothing':
        return get_actionitemnothing(intent_request)
    elif intent_name == 'actionitemnext':
        return next_action(intent_request)
    elif intent_name == 'updateactiondate':
        return update_action(intent_request)
    elif intent_name == 'countactionitems':
        return get_duedateactionitems(intent_request)
    elif intent_name == 'deleteaction':
        return delete_action(intent_request)
    elif intent_name == 'reassignaction':
        return reassign_actions(intent_request)
    elif intent_name == 'nextsecondinitiative':
        return nextsecondinitiatives(intent_request)
    elif intent_name == 'thirdinitiativenext':
        return nextthirdinitiatives(intent_request)
    elif intent_name == 'secondnothing':
        return getsecondnothing(intent_request)
    elif intent_name == 'thirdnothing':
        return getthirdnothing(intent_request)
    elif intent_name == 'initiativelike':
        return one_initiatives(intent_request)
    elif intent_name == 'changegoals':
        return changegoalstatus(intent_request)
    elif intent_name == 'listofactivestrategies':
        return activestrategyforgoal(intent_request)
    elif intent_name == 'allgoalshold':
        return onholdgoals(intent_request)
    elif intent_name == 'singlecgoalgetting':
        return single_goal_onhold(intent_request)
    elif intent_name == 'allgoalsunderreview':
        return underreviewgoals(intent_request)
    elif intent_name == 'changeoneinitiativestatus':
        return changefirstinitiativestatus(intent_request)
    elif intent_name == 'singlegoal':
        return get_singlegoal(intent_request)
    elif intent_name == 'singlelookupstrategy':
        return getactivesinglestrategygoal(intent_request)
    elif intent_name == 'userlogin':
        return get_login(intent_request)
    elif intent_name == 'changeinitiativesecond':
        return changesecondiniti(intent_request)
    elif intent_name == 'changeseconddate':
        return changesecondduedate(intent_request)
    elif intent_name == 'updatethirdinitiativestatus':
        return changethirdiniti(intent_request)
    elif intent_name == 'changethirdduedate':
        return changeduedatethird(intent_request)
  
    


   
   
     
    raise Exception('Intent with names ' + intent_name + ' not supported')


# --- Main handler ---


def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """
    # By default, treat the user request as coming from the America/New_York time zone.
    os.environ['TZ'] = 'America/New_York'
    time.tzset()
    logger.debug('event.bot.name={}'.format(event['bot']['name']))

    return dispatch(event)

