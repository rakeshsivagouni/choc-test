package com.hubster.dao;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubster.model.ActionModel;
import com.hubster.model.ActionsModel;
import com.hubster.model.ActiveStrategyGoalModel;
import com.hubster.model.CgoalModel;
import com.hubster.model.GoalModel;
import com.hubster.model.HubbiGoals;
import com.hubster.model.HubbiOnHold;
import com.hubster.model.LoginModel;
import com.hubster.model.TodoModel;
import com.hubster.request.Request;

public enum TodoDaoImpl implements TodoDao {
	instance;

	Logger logger = LoggerFactory.getLogger(TodoDaoImpl.class);

	public List<TodoModel> getAllTodos(Request req) {
		List<TodoModel> todoList = new ArrayList<TodoModel>();

		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.todo_challenge)) {

			ps.setString(1, req.getChassignid());

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				TodoModel em = new TodoModel();

				em.setChallenge(rs.getString("challenge"));

				System.out.println(em);

				todoList.add(em);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return todoList;
	}

	@Override
	public List<TodoModel> getUpdatedDate(Request request) {

		List<TodoModel> data = new ArrayList<TodoModel>();

		try (Connection conn = com.hubster.config.Database.connection();

				PreparedStatement ps1 = conn.prepareStatement(QueryConstants.todo_update)) {
			try {
				Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getFollowupp());
				java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

				ps1.setDate(1, sqlDate);

				ps1.setInt(2, request.getChidd());

				ps1.executeUpdate();

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public List<TodoModel> getDeleteTodo(Request request1) {
		List<TodoModel> dataa = new ArrayList<TodoModel>();

		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps4 = conn.prepareStatement(QueryConstants.todo_delete)) {

			ps4.setInt(1, request1.getChidd());

			ps4.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataa;
	}

	@Override
	public List<ActionModel> getActionItem(Request requests) {

		List<ActionModel> actionlist = new ArrayList<ActionModel>();

		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps5 = conn.prepareStatement(QueryConstants.get_action_item)) {
//			ps5.setString(1, requests.getChassignid());
			
			ps5.setString(1, requests.getStaffe_id());

			ResultSet rs = ps5.executeQuery();

			while (rs.next()) {

				ActionModel em = new ActionModel();

//				em.setActionitem(rs.getString("actionitem"));
				em.setChallenge(rs.getString("challenge"));
				em.setChid(rs.getInt("chid"));


				actionlist.add(em);
				System.out.println(actionlist);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actionlist;
	}

	@Override
	public List<ActionModel> updateActionItem(Request re) {
		List<ActionModel> updatedata = new ArrayList<ActionModel>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement psupdate = conn.prepareStatement(QueryConstants.update_action_item)) {

			psupdate.setString(1, re.getDuedatee());
			// psupdate.setInt(2, re.getActionid());
			psupdate.setInt(2, re.getChidd());
			psupdate.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return updatedata;
	}

	@Override
	public List<ActionModel> deleteActionItem(Request reqs) {
		List<ActionModel> deletedata = new ArrayList<ActionModel>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement psdelete = conn.prepareStatement(QueryConstants.delete_action_item)) {
			// psdelete.setInt(1, reqs.getActionid());
			psdelete.setInt(1, reqs.getChidd());
			psdelete.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return deletedata;

	}

	@Override
	public List<ActionsModel> actionItemDueDate(Request reqduedate) {
		List<ActionsModel> duedatedata = new ArrayList<ActionsModel>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement psduedate = conn.prepareStatement(QueryConstants.action_item_due_today)) {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			// Calendar calendarDM = Calendar.getInstance();
			// calendarDM.setTime(date);
			// calendarDM.set(Calendar.HOUR, 0);
			// calendarDM.set(Calendar.MINUTE, 0);
			// calendarDM.set(Calendar.SECOND, 0);
			String currentdate = formatter.format(date);
			// String currentdate =formatter.format(calendarDM.getTime());

			psduedate.setString(2, currentdate);

			System.out.println(currentdate);

			// psduedate.setInt(1, reqduedate.getActionid());
//			psduedate.setString(1, reqduedate.getChassignid());
			
			psduedate.setString(1, reqduedate.getStaffe_id());

			ResultSet rs = psduedate.executeQuery();

			while (rs.next()) {

				ActionsModel am = new ActionsModel();

				am.setTotal(rs.getInt("total"));
				duedatedata.add(am);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return duedatedata;
	}

	@Override
	public List<GoalModel> getAllGoals(Request getgoal) {

		List<GoalModel> goallist = new ArrayList<GoalModel>();

		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.getgoals)) {

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				GoalModel hgl = new GoalModel();
				// hgl.setCgoal_id(rs.getInt("cgoal_id"));
				hgl.setCgoalgroup(rs.getString("cgoalgroup"));

				System.out.println(hgl.getCgoal_id());
				System.out.println(hgl.getCgoalgroup());

				goallist.add(hgl);

				System.out.println("java object******** " + goallist);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return goallist;
	}

	@Override
	public List<HubbiGoals> updatestatusgoal(Request updategoal) {
		List<HubbiGoals> updategoallist = new ArrayList<HubbiGoals>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.updatestatusgoal)) {
			ps.setString(1, updategoal.getCorpgoalstatus());
			ps.setInt(2, updategoal.getCgoal_id());
			ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return updategoallist;
	}

	@Override
	public List<HubbiOnHold> getGoalsOnHold(Request getonhold) {
		List<HubbiOnHold> getgoalsonhold = new ArrayList<HubbiOnHold>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.goalsonhold)) {
			ps.setString(1, getonhold.getCcreatedby());
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				HubbiOnHold hg = new HubbiOnHold();
				hg.setCgoals(rs.getString("cgoals"));
				hg.setCgoal_id(rs.getInt("cgoal_id"));
				getgoalsonhold.add(hg);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return getgoalsonhold;
	}

	@Override
	public List<HubbiOnHold> getGoalsUnderReview(Request getunderreview) {
		List<HubbiOnHold> underreview = new ArrayList<HubbiOnHold>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.goalsunderreview)) {
			ps.setString(1, getunderreview.getCcreatedby());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				HubbiOnHold hgs = new HubbiOnHold();
				hgs.setCgoals(rs.getString("cgoals"));
				hgs.setCgoal_id(rs.getInt("cgoal_id"));
				underreview.add(hgs);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return underreview;
	}

	@Override
	public List<CgoalModel> getcgoals(Request cgoals) {
		List<CgoalModel> getcgoals = new ArrayList<CgoalModel>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.cgoals_getting)) {
			// ps.setString(1, cgoals.getUser_goal_id());
			// ps.setInt(1, cgoals.getCgoal_id());
			ps.setString(1, cgoals.getCgoalgroup());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CgoalModel hg = new CgoalModel();
				hg.setCgoals(rs.getString("cgoals"));
				hg.setCgoal_id(rs.getInt("cgoal_id"));
				getcgoals.add(hg);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return getcgoals;
	}

	/*@Override
	public List<HubbiGoals> reassign_goal(Request regoal) {
		List<HubbiGoals> reassign = new ArrayList<HubbiGoals>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.reassign_goal)) {
			ps.setString(1, regoal.getCcreatedby());
			ps.setInt(2, regoal.getCgoal_id());
			ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reassign;

	}
*/
	@Override
	public List<CgoalModel> getsingelgoal(Request singlegoal) {
		List<CgoalModel> getcgoals = new ArrayList<CgoalModel>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.singlegoal)) {
			// ps.setString(1, cgoals.getUser_goal_id());
			ps.setInt(1, singlegoal.getCgoal_id());
			// ps.setString(1, cgoals.getCgoalgroup());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CgoalModel hg = new CgoalModel();
				hg.setCgoals(rs.getString("cgoals"));
				hg.setCgoal_id(rs.getInt("cgoal_id"));
				getcgoals.add(hg);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return getcgoals;

	}

	@Override
	public List<ActiveStrategyGoalModel> getactivestrategygoal(Request singleactivegoal) {
		List<ActiveStrategyGoalModel> li = new ArrayList<ActiveStrategyGoalModel>();
		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.single_strategy_active_goal)) {
			ps.setInt(1, singleactivegoal.getId());
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ActiveStrategyGoalModel am = new ActiveStrategyGoalModel();
				am.setId(rs.getInt("id"));
				am.setCategory(rs.getString("category"));
				li.add(am);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return li;

	}

	@Override
	public List<LoginModel> getusername(Request loginname) {
		List<LoginModel> loginlist = new ArrayList<LoginModel>();

		try (Connection conn = com.hubster.config.Database.connection();
				PreparedStatement ps = conn.prepareStatement(QueryConstants.login)) {

			String user_name = loginname.getLoginuser();

			ps.setString(1, user_name);
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hashInBytes = md.digest(loginname.getPassword().getBytes(StandardCharsets.UTF_8));

			StringBuilder sb = new StringBuilder();
			for (byte b : hashInBytes) {
				sb.append(String.format("%02x", b));
			}

			String password1 = sb.toString();
			System.out.println(password1);

			ps.setString(2, password1);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				LoginModel loginmodel = new LoginModel();
				loginmodel.setLogin(rs.getString("login"));
				loginmodel.setPswd(rs.getString("pswd"));
				loginmodel.setLogin_id(rs.getInt("login_id"));

				String username = rs.getString("login");
				String password = rs.getString("pswd");
				int loginid = rs.getInt("login_id");

				System.out.println(loginid);
				if ((user_name.equals(username)) && (password1.equals(password))) {

					System.out.println("Username and Password exist");

				} else {
					System.out.println("Please Check Username and Password");

				}

				loginlist.add(loginmodel);
				System.out.println(username);
				System.out.println(password);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return loginlist;
	}

	@Override
	public List<HubbiGoals> reassign_goal(Request regoal) {
		// TODO Auto-generated method stub
		return null;
	}
}