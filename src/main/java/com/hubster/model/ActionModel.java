package com.hubster.model;

public class ActionModel {

	private int chid;
//	private String actionitem;
	
	private String challenge;
	
	public String getChallenge() {
		return challenge;
	}

	public void setChallenge(String challenge) {
		this.challenge = challenge;
	}

	private int total;

	public int getChid() {
		return chid;
	}

	public void setChid(int chid) {
		this.chid = chid;
	}

//	public String getActionitem() {
//		return actionitem;
//	}
//
//	public void setActionitem(String actionitem) {
//		this.actionitem = actionitem;
//	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

//	@Override
//	public String toString() {
//		return " [" + actionitem + "]";
//	}

}
