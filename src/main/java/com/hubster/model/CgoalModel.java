package com.hubster.model;

import java.util.List;

public class CgoalModel {

//	private String user_goal_id;
	private List<CgoalModel> goalmodel;
//	@Override
//	public String toString() {
//		return " [" + cgoals + "]";
//	}

	public int getCgoal_id() {
		return cgoal_id;
	}

	public void setCgoal_id(int cgoal_id) {
		this.cgoal_id = cgoal_id;
	}

	@Override
	public String toString() {
		return " " + cgoal_id + "." + cgoals + "";
	}

	private int cgoal_id;
	private String cgoals;


	public List<CgoalModel> getGoalmodel() {
		return goalmodel;
	}

	public void setGoalmodel(List<CgoalModel> goalmodel) {
		this.goalmodel = goalmodel;
	}

	public String getCgoals() {
		return cgoals;
	}

	public void setCgoals(String cgoals) {
		this.cgoals = cgoals;
	}

//	public String getUser_goal_id() {
//		return user_goal_id;
//	}
//
//	public void setUser_goal_id(String user_goal_id) {
//		this.user_goal_id = user_goal_id;
//	}

}
