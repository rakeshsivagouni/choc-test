package com.hubster.model;

import java.util.List;

public class GoalModel {

//	private String user_goal_id;
	
	private List<GoalModel> goalModel;
	
//	@Override
//	public String toString() {
//		return " [" + cgoal_id + "." + cgoalgroup + "]";
//	}

	private int cgoal_id;
	@Override
	public String toString() {
		return " " + cgoalgroup + "";
	}

	private String cgoalgroup;

	
	public int getCgoal_id() {
		return cgoal_id;
	}



	public void setCgoal_id(int cgoal_id) {
		this.cgoal_id = cgoal_id;
	}

//	
//	@Override
//	public String toString() {
//		return "["+ user_goal_id + "." + cgoalgroup + "]";
//	}



	public List<GoalModel> getGoalModel() {
		return goalModel;
	}

	public void setGoalModel(List<GoalModel> goalModel) {
		this.goalModel = goalModel;
	}

	public String getCgoalgroup() {
		return cgoalgroup;
	}

	public void setCgoalgroup(String cgoalgroup) {
		this.cgoalgroup = cgoalgroup;
	}

//	public String getUser_goal_id() {
//		return user_goal_id;
//	}
//
//	public void setUser_goal_id(String user_goal_id) {
//		this.user_goal_id = user_goal_id;
//	}

}
