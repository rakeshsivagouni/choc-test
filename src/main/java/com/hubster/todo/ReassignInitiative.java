package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hubster.dao.StrategyDaoImpl;
import com.hubster.model.ReassignInitiativeModel;
import com.hubster.model.StrategyCategory;
import com.hubster.request.Request;
import com.hubster.response.DialogAction;
import com.hubster.response.LexResponse;
import com.hubster.response.Message;
import com.hubster.response.ReassignInResponse;

public class ReassignInitiative implements RequestHandler<Request, ReassignInResponse> {

	private final StrategyDaoImpl strategyService = StrategyDaoImpl.instance;

	private ReassignInResponse getResponse() {
		return new ReassignInResponse();
	}

	@Override
	public ReassignInResponse handleRequest(Request input, Context context) {

		List<ReassignInitiativeModel> reassigninitiative = strategyService.getreassigninitiative(input);

		ReassignInResponse res = getResponse();
		if (!reassigninitiative.isEmpty()) {

			res.setGetreassign(reassigninitiative);

		} else {
		}
		 

		return res;
	}

}
