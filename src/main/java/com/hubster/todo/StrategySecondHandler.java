package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hubster.dao.StrategyDaoImpl;
import com.hubster.model.SecondInitiativeModel;
import com.hubster.model.StrategyCategory;
import com.hubster.request.Request;
import com.hubster.response.DialogAction;
import com.hubster.response.LexResponse;
import com.hubster.response.Message;
import com.hubster.response.SecondInitiativeResponse;

public class StrategySecondHandler implements RequestHandler<Request, SecondInitiativeResponse> {

	private final StrategyDaoImpl strategysecondService = StrategyDaoImpl.instance;

	private SecondInitiativeResponse getResponse() {
		return new SecondInitiativeResponse();
	}

	@Override
//	public LexResponse handleRequest(Request input, Context context) {
	public SecondInitiativeResponse  handleRequest(Request input, Context context) {
		List<SecondInitiativeModel> secondlist = strategysecondService.getsecondactivepriority(input);

		SecondInitiativeResponse res = getResponse();
		if (!secondlist.isEmpty()) {

			// res.setResCode(CommonContants.SUCCESS_CODE);
			// res.setResDesc(CommonContants.SUCCSS_DESC);
			res.setGetinitiatives(secondlist);

		} else {
			// res.setResCode(CommonContants.FAIL_CODE);
			// res.setResDesc(CommonContants.FAIL_DESC);
		}
//		Message message = new Message("PlainText", secondlist.toString());
//		DialogAction dialogueAction = new DialogAction("Close", "Fulfilled", message);
//
//		return new LexResponse(dialogueAction);
		return res;
	}

}
